package me.grapescan.trips

import me.grapescan.trips.data.token.AuthHeaderProvider

class StaticAuthHeaderProvider(private val header: String) : AuthHeaderProvider {

    override suspend fun <T> run(block: suspend (authToken: String) -> T): T = block(header)
}