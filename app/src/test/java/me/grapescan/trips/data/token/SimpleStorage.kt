package me.grapescan.trips.data.token

import me.grapescan.trips.data.Storage

class SimpleStorage(initialValue: String?) : Storage<String?> {

    private var data: String? = initialValue

    override suspend fun load() = data

    override suspend fun save(data: String?) {
        this.data = data
    }
}