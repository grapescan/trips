package me.grapescan.trips.data.token

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import kotlinx.coroutines.runBlocking
import me.grapescan.trips.data.Storage
import me.grapescan.common.ext.toUnit
import me.grapescan.trips.data.api.BlaBlaCarApi
import me.grapescan.trips.data.api.dto.GetTokenResponse
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.amshove.kluent.shouldEqual
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.HttpException
import retrofit2.Response

@RunWith(MockitoJUnitRunner.Silent::class)
class AuthHeaderProviderTest {

    companion object {

        private const val ACCESS_TOKEN = "test-token-718"
        private const val HEADER = "Bearer $ACCESS_TOKEN"
        private const val HEADER_OLD = "Bearer old-token-617"
        private val GET_TOKEN_RESPONSE = GetTokenResponse(ACCESS_TOKEN, "", "", "", "", emptyArray())
    }

    @Mock
    private lateinit var apiMock: BlaBlaCarApi

    @Mock
    private lateinit var storageMock: Storage<String?>

    @Test
    fun testEmptyStorage() = runBlocking {
        given(storageMock.load()).willReturn(null)
        given(apiMock.getToken(any())).willReturn(GET_TOKEN_RESPONSE)
        CachingAuthHeaderProvider(apiMock, storageMock).run { it shouldEqual HEADER }
        verify(storageMock).load()
        verify(storageMock).save(HEADER)
        verifyNoMoreInteractions(storageMock)
        verify(apiMock).getToken(any())
        verifyNoMoreInteractions(apiMock)
    }.toUnit()

    @Test
    fun testLoadFromStorage() = runBlocking {
        given(storageMock.load()).willReturn(HEADER)
        given(apiMock.getToken(any())).willReturn(GET_TOKEN_RESPONSE)
        CachingAuthHeaderProvider(apiMock, storageMock).run { it shouldEqual HEADER }
        verify(storageMock).load()
        verifyNoMoreInteractions(storageMock)
        verifyNoMoreInteractions(apiMock)
    }.toUnit()

    @Test
    fun testRefreshOnError() = runBlocking {
        val storage = SimpleStorage(HEADER_OLD)
        given(apiMock.getToken(any())).willReturn(GET_TOKEN_RESPONSE)
        CachingAuthHeaderProvider(apiMock, storage).run {
            if (it == HEADER_OLD) {
                throw HttpException(
                    Response.error<String>(
                        401,
                        ResponseBody.create(MediaType.get("text/plain"), "hello")
                    )
                )
            }
        }
        storage.load() shouldEqual HEADER
        verifyNoMoreInteractions(storageMock)
        verify(apiMock).getToken(any())
        verifyNoMoreInteractions(apiMock)
    }.toUnit()

}

