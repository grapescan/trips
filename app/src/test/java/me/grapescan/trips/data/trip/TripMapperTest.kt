package me.grapescan.trips.data.trip

import me.grapescan.trips.data.api.dto.MoneyDto
import me.grapescan.trips.data.api.dto.PlaceDto
import me.grapescan.trips.data.api.dto.TripDto
import me.grapescan.trips.data.api.dto.UserDto
import me.grapescan.trips.model.Trip
import org.amshove.kluent.shouldEqual
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner.Silent::class)
class TripMapperTest {

    companion object {
        private val TRIP_DTO = TripDto(
            UserDto("John Doe", "https://picture.com/123"),
            "12/03/2019 13:13",
            PlaceDto("Paris", "2094tj"),
            PlaceDto("London", "24ffg"),
            MoneyDto(277, "Euro", "EUR", "EUR 277")
        )

        private val TRIP = Trip("John Doe", "Paris - London", "EUR 277", "https://picture.com/123")
    }

    private val target = TripMapper()

    @Test
    fun testMapping() {
        target.map(TRIP_DTO) shouldEqual TRIP
    }

}