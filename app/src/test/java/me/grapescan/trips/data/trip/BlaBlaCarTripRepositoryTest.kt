package me.grapescan.trips.data.trip

import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import kotlinx.coroutines.runBlocking
import me.grapescan.common.Mapper
import me.grapescan.common.ext.toUnit
import me.grapescan.trips.ARRIVAL_LONDON
import me.grapescan.trips.ARRIVAL_MOSCOW
import me.grapescan.trips.DEPARTURE_PARIS
import me.grapescan.trips.StaticAuthHeaderProvider
import me.grapescan.trips.data.api.BlaBlaCarApi
import me.grapescan.trips.data.api.dto.*
import me.grapescan.trips.model.Trip
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.io.IOException
import java.util.*

@RunWith(MockitoJUnitRunner.Silent::class)
class BlaBlaCarTripRepositoryTest {

    companion object {
        private const val AUTH_HEADER = "Bearer: stub-token"
        private const val FORMAT_JSON = "json"
        private const val CURRENCY_EUR = "EUR"

        private val LOCALE = Locale.getDefault().toString()

        private val TRIP_DTO1 = TripDto(
            UserDto("John Doe", "https://picture.com/123"),
            "12/03/2019 13:13",
            PlaceDto("Paris", "2094tj"),
            PlaceDto("London", "24ffg"),
            MoneyDto(277, "Euro", "EUR", "EUR 277")
        )

        private val TRIP_DTO2 = TripDto(
            UserDto("Kevin Smith", "https://picture.com/123"),
            "12/03/2019 13:13",
            PlaceDto("Paris", "2094tj"),
            PlaceDto("London", "24ffg"),
            MoneyDto(1277, "Euro", "EUR", "EUR 1277")
        )

        private val TRIP_DTO3 = TripDto(
            UserDto("Mark Yong", "https://picture.com/123"),
            "12/03/2019 13:13",
            PlaceDto("Paris", "2094tj"),
            PlaceDto("London", "24ffg"),
            MoneyDto(4, "Euro", "EUR", "EUR 4")
        )

        private val TRIP1 = Trip("Trip 1", "111", "111", "111")
        private val TRIP2 = Trip("Trip 2", "222", "222", "222")
        private val TRIP3 = Trip("Trip 3", "333", "333", "333")
        private val API_RESPONSE_STUB = GetTripsResponse(
            listOf(TRIP_DTO1, TRIP_DTO2, TRIP_DTO3)
        )
    }

    @Mock
    private lateinit var apiMock: BlaBlaCarApi

    private val authHeaderProvider = StaticAuthHeaderProvider(AUTH_HEADER)

    @Mock
    private lateinit var tripMapperMock: Mapper<TripDto, Trip>

    private lateinit var target: BlaBlaCarTripRepository

    @Before
    fun setup() {
        runBlocking {
            given(
                apiMock.getTrips(
                    AUTH_HEADER,
                    FORMAT_JSON,
                    LOCALE,
                    CURRENCY_EUR,
                    DEPARTURE_PARIS,
                    ARRIVAL_LONDON
                )
            ).willReturn(API_RESPONSE_STUB)
            given(
                apiMock.getTrips(
                    AUTH_HEADER,
                    FORMAT_JSON,
                    LOCALE,
                    CURRENCY_EUR,
                    DEPARTURE_PARIS,
                    ARRIVAL_MOSCOW
                )
            ).willThrow(IOException("No connection"))
            given(tripMapperMock.invoke(TRIP_DTO1)).willReturn(TRIP1)
            given(tripMapperMock.invoke(TRIP_DTO2)).willReturn(TRIP2)
            given(tripMapperMock.invoke(TRIP_DTO3)).willReturn(TRIP3)
        }
        target = BlaBlaCarTripRepository(apiMock, authHeaderProvider, tripMapperMock)
    }

    @Test
    fun testTripLoadingSuccess() = runBlocking {
        target.getTrips(DEPARTURE_PARIS, ARRIVAL_LONDON) shouldEqual listOf(TRIP1, TRIP2, TRIP3)
        verify(apiMock).getTrips(
            AUTH_HEADER,
            FORMAT_JSON,
            LOCALE,
            CURRENCY_EUR,
            DEPARTURE_PARIS,
            ARRIVAL_LONDON
        )
        verifyNoMoreInteractions(apiMock)
        verify(tripMapperMock).invoke(TRIP_DTO1)
        verify(tripMapperMock).invoke(TRIP_DTO2)
        verify(tripMapperMock).invoke(TRIP_DTO3)
        verifyNoMoreInteractions(tripMapperMock)
    }.toUnit()

    @Test(expected = TripRepository.Error.ServerUnavailable::class)
    fun testTripLoadingError() = runBlocking {
        target.getTrips(DEPARTURE_PARIS, ARRIVAL_MOSCOW)
    }.toUnit()
}