package me.grapescan.trips.ui.trip

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jraska.livedata.test
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyNoMoreInteractions
import kotlinx.coroutines.runBlocking
import me.grapescan.common.ext.toUnit
import me.grapescan.trips.*
import me.grapescan.trips.data.trip.TripRepository
import org.amshove.kluent.`should be instance of`
import org.amshove.kluent.shouldEqual
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner.Silent::class)
class TripListViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var tripRepositoryMock: TripRepository

    private lateinit var target: TripListViewModel

    @Before
    fun setup() {
        runBlocking {
            given(tripRepositoryMock.getTrips(DEPARTURE_PARIS, ARRIVAL_LUXEMBOURG)).willReturn(PARIS_LUXEMBOURG_TRIPS)
            given(
                tripRepositoryMock.getTrips(
                    DEPARTURE_PARIS,
                    ARRIVAL_MOSCOW
                )
            ).willThrow(TripRepository.Error.ServerUnavailable)
            given(tripRepositoryMock.getTrips(DEPARTURE_PARIS, ARRIVAL_LONDON)).willReturn(emptyList())
        }
    }

    @Test
    fun testTitle() = runBlocking {
        TripListViewModel(DEPARTURE_PARIS, ARRIVAL_LUXEMBOURG, tripRepositoryMock).title.test()
            .awaitValue()
            .assertHasValue()
            .assertValue { it == "Paris — Luxembourg" }
    }.toUnit()

    @Test
    fun testInitialState() = runBlocking {
        TripListViewModel(DEPARTURE_PARIS, ARRIVAL_LUXEMBOURG, tripRepositoryMock).state.test()
            .awaitValue()
            .awaitNextValue()
            .valueHistory().run {
                first() shouldEqual TripListViewModel.ViewState.Loading
                last() `should be instance of` TripListViewModel.ViewState.Content::class
            }
        verify(tripRepositoryMock).getTrips(DEPARTURE_PARIS, ARRIVAL_LUXEMBOURG)
        verifyNoMoreInteractions(tripRepositoryMock)
    }.toUnit()

    @Test
    fun testLoadingSuccess() = runBlocking {
        TripListViewModel(DEPARTURE_PARIS, ARRIVAL_LUXEMBOURG, tripRepositoryMock).state.test()
            .awaitValue()
            .awaitNextValue()
            .valueHistory().last() shouldEqual TripListViewModel.ViewState.Content(PARIS_LUXEMBOURG_TRIPS)
        verify(tripRepositoryMock).getTrips(DEPARTURE_PARIS, ARRIVAL_LUXEMBOURG)
        verifyNoMoreInteractions(tripRepositoryMock)
    }.toUnit()

    @Test
    fun testLoadingFailed() = runBlocking {
        TripListViewModel(DEPARTURE_PARIS, ARRIVAL_MOSCOW, tripRepositoryMock).state.test()
            .awaitValue()
            .awaitNextValue()
            .valueHistory().last() shouldEqual TripListViewModel.ViewState.Error(TripListViewModel.ViewState.Error.Code.UNKNOWN)
        verify(tripRepositoryMock).getTrips(DEPARTURE_PARIS, ARRIVAL_MOSCOW)
        verifyNoMoreInteractions(tripRepositoryMock)
    }.toUnit()

    @Test
    fun testEmptyResult() = runBlocking {
        TripListViewModel(DEPARTURE_PARIS, ARRIVAL_LONDON, tripRepositoryMock).state.test()
            .awaitValue()
            .awaitNextValue()
            .valueHistory().last() shouldEqual TripListViewModel.ViewState.Empty
        verify(tripRepositoryMock).getTrips(DEPARTURE_PARIS, ARRIVAL_LONDON)
        verifyNoMoreInteractions(tripRepositoryMock)
    }.toUnit()
}



