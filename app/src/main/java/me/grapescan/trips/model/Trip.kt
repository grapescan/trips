package me.grapescan.trips.model

data class Trip(
    val title: String,
    val subtitle: String,
    val price: String,
    val picture: String
)
