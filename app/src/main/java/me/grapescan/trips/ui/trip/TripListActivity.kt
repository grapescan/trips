package me.grapescan.trips.ui.trip

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import me.grapescan.common.ext.StringExtra
import me.grapescan.common.viewstate.ListStateController
import me.grapescan.trips.R
import me.grapescan.trips.databinding.ActivityTripListBinding
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class TripListActivity : AppCompatActivity() {

    companion object {

        private var Intent.departure by StringExtra()
        private var Intent.arrival by StringExtra()

        fun createIntent(context: Context, departure: String, arrival: String) =
            Intent(context, TripListActivity::class.java).apply {
                this.departure = departure
                this.arrival = arrival
            }
    }

    private val viewModel: TripListViewModel by inject(parameters = { parametersOf(intent.departure, intent.arrival) })
    private lateinit var binding: ActivityTripListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_trip_list)
        binding.run {
            coordinator.tag = ListStateController(
                binding.root,
                contentViewIds = intArrayOf(R.id.content),
                emptyViewIds = intArrayOf(R.id.placeholderEmpty),
                errorViewIds = intArrayOf(R.id.errorMessage),
                loadingViewIds = intArrayOf(R.id.progress)
            )
            // this is required to change title later; see https://stackoverflow.com/a/35430590/252561
            toolbar.title = ""
            setSupportActionBar(toolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            content.run {
                adapter = TripAdapter()
                layoutManager = LinearLayoutManager(this@TripListActivity)
            }

        }
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
    }
}
