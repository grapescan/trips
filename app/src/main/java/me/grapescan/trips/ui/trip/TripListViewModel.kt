package me.grapescan.trips.ui.trip

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import me.grapescan.trips.data.trip.TripRepository
import me.grapescan.trips.model.Trip

class TripListViewModel(
    departure: String,
    arrival: String,
    tripRepository: TripRepository
) : ViewModel() {

    val title: LiveData<String> = MutableLiveData<String>("$departure — $arrival")

    private val mutableState: MutableLiveData<ViewState> by lazy {
        MutableLiveData<ViewState>(ViewState.Loading).also {
            viewModelScope.launch(Dispatchers.IO) {
                mutableState.postValue(
                    try {
                        val trips = tripRepository.getTrips(departure, arrival)
                        if (trips.isEmpty()) {
                            ViewState.Empty
                        } else {
                            ViewState.Content(trips)
                        }
                    } catch (error: TripRepository.Error) {
                        ViewState.Error(
                            when (error) {
                                TripRepository.Error.ServerUnavailable -> ViewState.Error.Code.SERVER_UNAVAILABLE
                                TripRepository.Error.Unknown -> ViewState.Error.Code.UNKNOWN
                            }
                        )
                    }
                )
            }
        }
    }

    val state: LiveData<ViewState> = mutableState

    sealed class ViewState {

        object Loading : ViewState()

        object Empty : ViewState()

        data class Error(val code: Code) : ViewState() {

            enum class Code {
                SERVER_UNAVAILABLE, UNKNOWN
            }
        }

        data class Content(val items: List<Trip>) : ViewState()
    }
}
