package me.grapescan.trips.ui.main

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import me.grapescan.trips.R
import me.grapescan.trips.ui.trip.TripListActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val departure = findViewById<TextView>(R.id.departure)
        val arrival = findViewById<TextView>(R.id.arrival)
        findViewById<Button>(R.id.search).setOnClickListener {
            startActivity(
                TripListActivity.createIntent(
                    this,
                    departure.text.toString(),
                    arrival.text.toString()
                )
            )
        }
    }
}
