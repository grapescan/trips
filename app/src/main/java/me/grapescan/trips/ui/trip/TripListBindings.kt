package me.grapescan.trips.ui.trip

import android.content.res.Resources
import android.widget.ImageView
import android.widget.TextView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import me.grapescan.common.viewstate.ListStateController
import me.grapescan.common.viewstate.ViewStateController
import me.grapescan.trips.R
import me.grapescan.trips.ui.GlideApp

@BindingAdapter("state")
fun setState(layout: CoordinatorLayout, state: TripListViewModel.ViewState) = with(layout.tag as ViewStateController) {
    switchState(
        when (state) {
            TripListViewModel.ViewState.Empty -> ListStateController.EMPTY
            TripListViewModel.ViewState.Loading -> ListStateController.LOADING
            is TripListViewModel.ViewState.Content -> ListStateController.CONTENT
            is TripListViewModel.ViewState.Error -> ListStateController.ERROR
        }
    )
}

@BindingAdapter("state")
fun setState(view: TextView, state: TripListViewModel.ViewState) = when (state) {
    is TripListViewModel.ViewState.Error -> view.text = getErrorMessage(view.context.resources, state.code)
    else -> Unit
}

fun getErrorMessage(resources: Resources, code: TripListViewModel.ViewState.Error.Code): String = resources.getString(
    when (code) {
        TripListViewModel.ViewState.Error.Code.UNKNOWN -> R.string.error_unknown
        TripListViewModel.ViewState.Error.Code.SERVER_UNAVAILABLE -> R.string.error_server_unavailable
    }
)

@BindingAdapter("state")
fun setState(view: RecyclerView, state: TripListViewModel.ViewState) = when (state) {
    is TripListViewModel.ViewState.Content -> (view.adapter as? TripAdapter)?.submitList(state.items)
    else -> Unit
}

@BindingAdapter("url")
fun setUrl(view: ImageView, url: String) = GlideApp.with(view)
    .load(url)
    .into(view)
