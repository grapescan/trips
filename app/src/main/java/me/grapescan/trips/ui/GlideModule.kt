package me.grapescan.trips.ui

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * This class is required by Glide 4+ to build generated API.
 */
@GlideModule
class GlideModule : AppGlideModule()
