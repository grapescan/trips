package me.grapescan.trips.ui.trip

import androidx.recyclerview.widget.DiffUtil
import me.grapescan.common.recyclerview.DataBindingAdapter
import me.grapescan.trips.R
import me.grapescan.trips.model.Trip

class TripAdapter : DataBindingAdapter<Trip>(DIFF_CALLBACK) {

    companion object {

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Trip>() {

            override fun areContentsTheSame(oldItem: Trip, newItem: Trip) = oldItem == newItem

            override fun areItemsTheSame(oldItem: Trip, newItem: Trip) = oldItem == newItem
        }
    }

    override fun getItemViewType(position: Int) = R.layout.item_trip
}
