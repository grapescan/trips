package me.grapescan.trips

import android.app.Application
import me.grapescan.trips.data.Storage
import me.grapescan.common.Mapper
import me.grapescan.trips.data.api.BlaBlaCarApi
import me.grapescan.trips.data.api.dto.TripDto
import me.grapescan.trips.data.token.AuthHeaderProvider
import me.grapescan.trips.data.token.CachingAuthHeaderProvider
import me.grapescan.trips.data.token.PersistentAuthHeaderStorage
import me.grapescan.trips.data.trip.BlaBlaCarTripRepository
import me.grapescan.trips.data.trip.TripMapper
import me.grapescan.trips.data.trip.TripRepository
import me.grapescan.trips.model.Trip
import me.grapescan.trips.ui.trip.TripListViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@App)
            androidFileProperties()
            modules(module {
                single {
                    OkHttpClient.Builder()
                        .addInterceptor(HttpLoggingInterceptor().apply {
                            level = HttpLoggingInterceptor.Level.BODY
                        })
                        .build()
                }
                single {
                    Retrofit.Builder()
                        .baseUrl("https://edge.blablacar.com/")
                        .client(get<OkHttpClient>())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()
                }
                single<Storage<String?>> { PersistentAuthHeaderStorage(get()) }
                single<BlaBlaCarApi> { get<Retrofit>().create(BlaBlaCarApi::class.java) }
                single<AuthHeaderProvider> { CachingAuthHeaderProvider(get(), get()) }
                single<Mapper<TripDto, Trip>> { TripMapper() }
                single<TripRepository> {
                    BlaBlaCarTripRepository(
                        get(),
                        get(),
                        get()
                    )
                }
                factory { (departure: String, arrival: String) -> TripListViewModel(departure, arrival, get()) }
            })
        }
    }
}
