package me.grapescan.trips.data.api.dto

import com.google.gson.annotations.SerializedName

data class GetTokenRequest(
    @SerializedName("grant_type") val grantType: String,
    @SerializedName("client_id") val clientId: String,
    @SerializedName("client_secret") val clientSecret: String,
    @SerializedName("scopes") val scopes: Array<String>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GetTokenRequest

        if (grantType != other.grantType) return false
        if (clientId != other.clientId) return false
        if (clientSecret != other.clientSecret) return false
        if (!scopes.contentEquals(other.scopes)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = grantType.hashCode()
        result = 31 * result + clientId.hashCode()
        result = 31 * result + clientSecret.hashCode()
        result = 31 * result + scopes.contentHashCode()
        return result
    }
}