package me.grapescan.trips.data.token

import me.grapescan.trips.data.Storage
import me.grapescan.trips.data.api.BlaBlaCarApi
import me.grapescan.trips.data.api.dto.GetTokenRequest
import retrofit2.HttpException

class CachingAuthHeaderProvider(
    private val api: BlaBlaCarApi,
    private val storage: Storage<String?>
) : AuthHeaderProvider {

    companion object {
        private const val GRANT_TYPE_CLIENT_CREDENTIALS = "client_credentials"
        private const val CLIENT_ID = "android-technical-tests"
        @Suppress("SpellCheckingInspection")
        private const val CLIENT_SECRET = "Y1oAL3QdPfVhGOWj3UeDjo3q02Qwhvrj"
        private const val SCOPE_DEFAULT = "DEFAULT"
        private const val SCOPE_TRIP_DRIVER = "SCOPE_TRIP_DRIVER"
        private const val SCOPE_INTERNAL_CLIENT = "SCOPE_INTERNAL_CLIENT"
    }

    private suspend fun getHeader() = storage.load() ?: "Bearer ${requestNewToken().accessToken}"
        .also { storage.save(it) }

    private suspend fun requestNewToken() = api.getToken(
        GetTokenRequest(
            GRANT_TYPE_CLIENT_CREDENTIALS,
            CLIENT_ID,
            CLIENT_SECRET,
            arrayOf(
                SCOPE_DEFAULT,
                SCOPE_INTERNAL_CLIENT,
                SCOPE_TRIP_DRIVER
            )
        )
    )

    override suspend fun <T> run(block: suspend (authHeader: String) -> T): T =
        try {
            block(getHeader())
        } catch (exception: HttpException) {
            if (exception.code() == 401) {
                storage.save(null)
                block(getHeader())
            } else {
                throw exception
            }
        }
}
