package me.grapescan.trips.data.trip

import me.grapescan.common.Mapper
import me.grapescan.trips.data.api.dto.TripDto
import me.grapescan.trips.model.Trip

class TripMapper : Mapper<TripDto, Trip> {
    override fun map(input: TripDto) = Trip(
        input.user.displayName,
        "${input.departurePlace.cityName} - ${input.arrivalPlace.cityName}",
        input.price.string_value,
        input.user.pictureUrl
    )
}
