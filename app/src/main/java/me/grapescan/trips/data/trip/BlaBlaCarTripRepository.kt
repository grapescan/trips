package me.grapescan.trips.data.trip

import me.grapescan.common.Mapper
import me.grapescan.trips.data.api.BlaBlaCarApi
import me.grapescan.trips.data.api.dto.TripDto
import me.grapescan.trips.data.token.AuthHeaderProvider
import me.grapescan.trips.model.Trip
import java.io.IOException
import java.net.UnknownHostException
import java.util.*

class BlaBlaCarTripRepository(
    private val api: BlaBlaCarApi,
    private val authHeaderProvider: AuthHeaderProvider,
    private val tripMapper: Mapper<TripDto, Trip>
) : TripRepository {

    companion object {
        private const val FORMAT_JSON = "json"
        private const val CURRENCY_EURO = "EUR"
    }

    override suspend fun getTrips(departure: String, arrival: String) = authHeaderProvider.run { authHeader ->
        try {
            api.getTrips(
                authHeader,
                FORMAT_JSON,
                Locale.getDefault().toString(),
                CURRENCY_EURO,
                departure,
                arrival
            ).trips
                .map(tripMapper)
        } catch (error: IOException) {
            throw when (error) {
                is UnknownHostException -> TripRepository.Error.ServerUnavailable
                else -> TripRepository.Error.ServerUnavailable
            }
        }
    }

}
