package me.grapescan.trips.data.token

import android.annotation.SuppressLint
import android.content.Context
import android.preference.PreferenceManager
import me.grapescan.trips.data.Storage

class PersistentAuthHeaderStorage(context: Context) : Storage<String?> {

    companion object {
        private const val PREF_AUTH_HEADER = "auth_header"
    }

    private val prefs = PreferenceManager.getDefaultSharedPreferences(context)

    override suspend fun load(): String? = prefs.getString(PREF_AUTH_HEADER, null)

    @SuppressLint("ApplySharedPref")
    override suspend fun save(data: String?) {
        prefs.edit().putString(PREF_AUTH_HEADER, data).commit()
    }

}
