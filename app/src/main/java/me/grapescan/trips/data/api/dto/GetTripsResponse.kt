package me.grapescan.trips.data.api.dto

import com.google.gson.annotations.SerializedName

data class GetTripsResponse(
    @SerializedName("trips") val trips: List<TripDto>
)