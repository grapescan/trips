package me.grapescan.trips.data.token

interface AuthHeaderProvider {

    suspend fun <T> run(block: suspend (authToken: String) -> T): T
}
