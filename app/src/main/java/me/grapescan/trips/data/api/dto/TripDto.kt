package me.grapescan.trips.data.api.dto

import com.google.gson.annotations.SerializedName

data class TripDto(
    @SerializedName("user") val user: UserDto,
    @SerializedName("departure_date") val departureDate: String,
    @SerializedName("departure_place") val departurePlace: PlaceDto,
    @SerializedName("arrival_place") val arrivalPlace: PlaceDto,
    @SerializedName("price") val price: MoneyDto
)