package me.grapescan.trips.data.api.dto

import com.google.gson.annotations.SerializedName

data class UserDto(
    @SerializedName("display_name") val displayName: String,
    @SerializedName("picture") val pictureUrl: String
)