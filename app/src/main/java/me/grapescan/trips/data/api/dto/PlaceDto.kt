package me.grapescan.trips.data.api.dto

import com.google.gson.annotations.SerializedName

data class PlaceDto(
    @SerializedName("city_name") val cityName: String,
    @SerializedName("address") val address: String
)