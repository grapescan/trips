package me.grapescan.trips.data.api.dto

import com.google.gson.annotations.SerializedName

data class MoneyDto(
    @SerializedName("value") val value: Int,
    @SerializedName("currency") val currency: String,
    @SerializedName("symbol") val symbol: String,
    @SerializedName("string_value") val string_value: String
)