package me.grapescan.trips.data.trip

import me.grapescan.trips.model.Trip

interface TripRepository {

    @Throws(Error.ServerUnavailable::class)
    suspend fun getTrips(departure: String, arrival: String): List<Trip>

    sealed class Error : Exception() {

        object ServerUnavailable : Error()

        object Unknown : Error()
    }
}
