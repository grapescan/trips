package me.grapescan.trips.data.api

import me.grapescan.trips.data.api.dto.GetTokenRequest
import me.grapescan.trips.data.api.dto.GetTokenResponse
import me.grapescan.trips.data.api.dto.GetTripsResponse
import retrofit2.http.*
import java.io.IOException

interface BlaBlaCarApi {

    @POST("token")
    suspend fun getToken(@Body request: GetTokenRequest): GetTokenResponse

    @Throws(IOException::class)
    @GET("api/v2/trips")
    suspend fun getTrips(
        @Header("Authorization") authHeader: String, @Query("_format") format: String, @Query("locale") locale: String,
        @Query("cur") currency: String, @Query("fn") departure: String, @Query("tn") arrival: String
    ): GetTripsResponse
}














