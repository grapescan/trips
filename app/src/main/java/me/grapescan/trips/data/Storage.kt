package me.grapescan.trips.data

interface Storage<T> {
    suspend fun load(): T
    suspend fun save(data: T)
}
