package me.grapescan.trips.data.api.dto

import com.google.gson.annotations.SerializedName

data class GetTokenResponse(
    @SerializedName("access_token") val accessToken: String,
    @SerializedName("token_type") val tokenType: String,
    @SerializedName("issued_at") val issued_at: String,
    @SerializedName("expires_in") val expiresIn: String,
    @SerializedName("client_id") val clientId: String,
    @SerializedName("scopes") val scopes: Array<String>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GetTokenResponse

        if (accessToken != other.accessToken) return false
        if (tokenType != other.tokenType) return false
        if (issued_at != other.issued_at) return false
        if (expiresIn != other.expiresIn) return false
        if (clientId != other.clientId) return false
        if (!scopes.contentEquals(other.scopes)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = accessToken.hashCode()
        result = 31 * result + tokenType.hashCode()
        result = 31 * result + issued_at.hashCode()
        result = 31 * result + expiresIn.hashCode()
        result = 31 * result + clientId.hashCode()
        result = 31 * result + scopes.contentHashCode()
        return result
    }
}