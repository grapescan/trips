package me.grapescan.common.viewstate

import android.view.View

class ListStateController(
    rootView: View,
    contentViewIds: IntArray = intArrayOf(),
    errorViewIds: IntArray = intArrayOf(),
    emptyViewIds: IntArray = intArrayOf(),
    loadingViewIds: IntArray = intArrayOf(),
    defaultState: Int = LOADING
) : ViewStateController(rootView, stateMap(contentViewIds, errorViewIds, emptyViewIds, loadingViewIds), defaultState) {

    companion object {

        const val LOADING = 0
        const val EMPTY = 1
        const val ERROR = 2
        const val CONTENT = 3

        private fun stateMap(
            contentViewIds: IntArray, errorViewIds: IntArray, emptyViewIds: IntArray,
            loadingViewIds: IntArray = intArrayOf()
        ) = mapOf(
            LOADING to loadingViewIds,
            EMPTY to emptyViewIds,
            ERROR to errorViewIds,
            CONTENT to contentViewIds
        )
    }
}
