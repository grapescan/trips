package me.grapescan.common.viewstate

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.view.View

open class ViewStateController {

    companion object {
        private const val MIN_PROGRESS_SHOW_TIME = 500
        private const val PROGRESS_SHOW_DELAY = 500
        private const val CROSS_FADE_DURATION = 300L
    }

    private val states: Map<Int, List<View>>
    private val handler: Handler
    private val defaultState: Int
    private var currentState: Int = -1
    private var minProgressShowTime: Int
    private var progressShowDelay: Int
    private var lastStateSwitchTimestamp: Long = 0
    private var switchStateCallback: Runnable? = null

    private var animated = false

    constructor(rootView: View, states: Map<Int, IntArray>, defaultState: Int) {
        this.states = states.mapValues { entry ->
            entry.value.map { viewId ->
                rootView.findViewById<View>(viewId)
                    ?: throw IllegalArgumentException("View with id $viewId was not found")
            }
        }
        this.defaultState = defaultState
        this.handler = Handler(Looper.getMainLooper())
        this.minProgressShowTime = MIN_PROGRESS_SHOW_TIME
        this.progressShowDelay = PROGRESS_SHOW_DELAY
        setContentState(defaultState)
    }

    fun setAnimated(animated: Boolean): ViewStateController {
        this.animated = animated
        return this
    }

    fun setMinProgressShowTime(minProgressShowTime: Int): ViewStateController {
        this.minProgressShowTime = minProgressShowTime
        return this
    }

    fun setProgressShowDelay(progressShowDelay: Int): ViewStateController {
        this.progressShowDelay = progressShowDelay
        return this
    }

    open fun switchState(state: Int) {
        handler.removeCallbacks(switchStateCallback)

        if (currentState == state) {
            return
        }

        switchStateCallback = Runnable { setContentState(state) }
        val elapsedTime = SystemClock.elapsedRealtime() - lastStateSwitchTimestamp
        when (state) {
            defaultState -> handler.postDelayed(switchStateCallback, progressShowDelay.toLong())
            else -> handler.postDelayed(switchStateCallback, Math.max(0, minProgressShowTime - elapsedTime))
        }
    }

    fun setContentState(state: Int) {
        currentState = state
        states.forEach { hideView(it.value) }
        states.forEach { if (it.key == state) showView(it.value) }
        lastStateSwitchTimestamp = SystemClock.elapsedRealtime()
    }

    private fun showView(view: View) {
        if (view.visibility != View.VISIBLE) {
            if (animated) {
                view.visibility = View.VISIBLE
                view.alpha = 0f
                view.animate()
                    .alpha(1f)
                    .setDuration(CROSS_FADE_DURATION)
                    .setListener(null)
                    .start()
            } else {
                view.visibility = View.VISIBLE
            }
        }
    }

    private fun showView(views: List<View>) {
        views.forEach { view -> showView(view) }
    }

    private fun hideView(view: View) {
        if (view.visibility == View.VISIBLE) {
            if (animated) {
                view.animate()
                    .alpha(0f)
                    .setDuration(CROSS_FADE_DURATION)
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationEnd(animation: Animator) {
                            view.visibility = View.GONE
                        }
                    })
                    .start()
            } else {
                view.visibility = View.GONE
            }
        }
    }

    private fun hideView(views: List<View>) {
        views.forEach { view -> hideView(view) }
    }
}
