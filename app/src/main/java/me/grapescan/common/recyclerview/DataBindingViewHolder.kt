package me.grapescan.common.recyclerview

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import me.grapescan.trips.BR

class DataBindingViewHolder<T>(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: T) = binding.run {
        setVariable(BR.item, item)
        executePendingBindings()
    }
}
